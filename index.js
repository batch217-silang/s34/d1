const express = require("express");
const app = express();
const port = 3000;
// middleware 
app.use(express.json());
app.use(express.urlencoded({extended:true}));


/*[SECTION] Routes 
= holds tha HTTP methods
= endpoints: /login , /posts , /todos , /
*/

// GET METHOD (EXPRESS)
app.get("/", (req, res) => {
	res.send("Hello World!");
})


app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint!");
})


app.get("/cely", (req, res) => {
	res.send("Hello! I am Cely Silang");
})

// POST METHOD
app.post("/hello" , (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})

let users = [];
app.post("/signup" , (req, res) =>  {
	console.log(req.body);
	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered`);
	}else{
		res.send("Please input BOTH username and password");
	}
})


// PUT METHOD
app.put("/change-password", (req, res) => {
	let message;
	for (let i = 0; i < users.length; i++){
		if(req.body.username == users[i].username){
			users[i].password = req.body.password
			message = `User ${req.body.username} password has been updated.`
			break;
		}else{
			message = "User does not exist."
		}
	}
	res.send(message);
})




// DISCUSSION - ACTIVITY
app.get("/home", (req, res) => {
	res.send("Welcome to the homepage")
});


app.get("/users" , (req, res) => {
	res.send(users);
})





// add.delete("/delete-user", (req, res) =>{
// 	let message;

// 	if(users.length != 0){
// 		for(let i = 0; i < users.length; i++){
// 			if(req.body.username == users[i].username){
// 				users.splice(users[i], 1);
// 				message = `User ${req.body.username} has been deleted`;
// 				break;
// 			}else{
// 				message = "User does not exist.";
// 			}
// 		}
// 	}

// 	res.send(message);
// })


app.delete("/delete-user", (req, res) =>{
	let message;

	if(users.length != 0){
		for(let i = 0; i < users.length; i++){
			if(req.body.username == users[i].username){
				users.splice(users[i], 1);
				message = `User ${req.body.username} has been deleted`;
				break;
			}else{
				message = "User does not exist.";
			}
		}
	}

	res.send(message);
})




app.listen(port, () => console.log(`Server is running at port ${port}`));